<?php namespace API\V1\Repositories\Eloquent;

use API\Domain\User\UserRepositoryInterface;
use API\V1\Models\Organization;
use API\V1\Transformers\UserTransformer;
use API\V1\Models\User;

class UsersRepository extends BaseRepository implements UserRepositoryInterface
{

    function __construct(User $model, UserTransformer $transformer)
    {
        parent::__construct($model, $transformer);
    }

    public function findByEmail($email)
    {
        return User::where('email', strtolower(trim($email)))->first();
    }

    public function findByUsernameWithinOrg($orgID, $username)
    {
        return Organization::findOrFail($orgID)->users()->where('username', $username)->firstOrFail();
    }

    public function updateUserInfo($user)
    {
        $namesFromInput = \Input::only('first_name', 'last_name');
//        if( is_null($user->username) ) {
//            $full_name = $namesFromInput['first_name'] . ' ' . $namesFromInput['last_name'];
//            $user->username = get_username($full_name);
//        }
        $user->first_name = $namesFromInput['first_name'];
        $user->last_name = $namesFromInput['last_name'];
        $user->save();
    }

    public function getByCode($code)
    {
        $user = $this->model->where('activation_code', $code)->firstOrFail();
        return $user;
    }

    public function getByUsername($username)
    {
        $user = $this->model->where('username', $username)->firstOrFail();
        return $user;
    }
    
    public function findByIDOrUsername($idOrUsername)
    {
        $query = $this->model->where('username', $idOrUsername);
        if( is_numeric($idOrUsername) ) {
            $query->orWhere('id', $idOrUsername);
        }
        return $query->firstOrFail();
    }

    public function findByIDOrUsernameWithinOrg($orgID, $idOrUsername)
    {
        $query = Organization::findOrFail($orgID)->users()->where('username', $idOrUsername);
        if( is_numeric($idOrUsername) ) {
            $query->orWhere('users.id', $idOrUsername);
        }
        return $query->firstOrFail();
    }

    public function search($needle)
    {
        if (!is_null($needle)) {
            $this->query = $this->query()->where(function ($q) use ($needle) {
                foreach ($this->model->queryable as $field) {
                    $q->orWhere($field, 'ILIKE', '%' . $needle . '%');
                }
                $q->orWhere(\DB::raw("CONCAT(first_name, ' ', last_name)"), 'ILIKE', '%' . $needle . '%');
            });
        }
        return $this;
    }

}