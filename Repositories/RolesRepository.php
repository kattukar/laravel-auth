<?php namespace API\V1\Repositories\Eloquent;

use API\V1\Models\Role;
use API\V1\Transformers\RoleTransformer;

class RolesRepository extends BaseRepository
{
    function __construct(Role $model, RoleTransformer $transformer)
    {
        parent::__construct($model, $transformer);
    }

    public function getAllRoles($params)
    {
        return $this->buildPaginated($this->query(), $params);
    }

}