<?php namespace API\V1\Repositories\Eloquent;

use Illuminate\Http\Request;
use API\Exceptions\CustomSentryException;
use API\V1\Models\User;
use API\Domain\Observers\Accounts\Auth\SendActivationEmail;
use API\V1\Transformers\UserTransformer;
use Cartalyst\Sentry\Throttling\Eloquent\Throttle;
use Cartalyst\Sentry\Throttling\UserSuspendedException;

class AuthRepository
{
    /**
     * @var \Sentry
     */
    protected $provider;

    function __construct(User $model, UserTransformer $transformer)
    {
        $this->provider = \App::make('sentry');
        $this->model = $model;
        $this->transformer = $transformer;
    }

    /**
     * @param Request $request
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    function register($request)
    {
        $details = $request->only('email', 'password', 'first_name', 'last_name');
        if (!isset($details['username'])) {
            $details['username'] = strtolower($details['first_name'] . '.' . $details['last_name']);
        }

        while (\Validator::make($details, ['username' => 'required|unique:users'])->fails()) {
            $details['username'] = strtolower($details['first_name'] . '.' . $details['last_name']) . rand(1, 99999);
        }

        \DB::beginTransaction();
        $user = $this->provider->register($details, true);
        \Event::fire('user.registered', ['user' => $user, 'password' => $details['password']]);
        \DB::commit();
        return $user;
    }

    /**
     * @param Request $request
     */
    function login($request, $social = false)
    {
        //if a user with non-existent email is trying to log in, throw a new UserEmailNotFoundException
        if (!User::where('email', $request->input('email'))->first()) {
            throw new CustomSentryException('This email address is not recognized by Tallyfy.');
        }

        $throttle = Throttle::where('ip_address', $request->ip())->where('email',
            $request->input('email'))->first() ?: Throttle::create([
            'ip_address' => $request->ip(),
            'email' => $request->input('email')
        ]);
        $throttle->setSuspensionTime(12 * 60);
        if ($throttle->isSuspended()) {
            throw new UserSuspendedException();
        }

        //sometimes if a user logged in with different IPs, multiple entries will exist in the throttle table
        $throttleUsersByEmail = Throttle::where('email', $request->input('email'))->get();

        //if there are multiple entries for that email, apply unsuspend() to them as well
        $throttleUsersByEmail->map(function ($throttle) {
            $throttle->unsuspend();
        });
        if ($social) {
            $user = $this->provider->findUserByLogin($request->only('email'));
        } else {
            $user = $this->provider->findUserByCredentials($request->only('email', 'password'));
        }

        //dd($user);EXIT;
        if ($user) {
            //clear all login attempts for all users with that email
            $throttleUsersByEmail->map(function ($throttle) {
                $throttle->clearLoginAttempts();
            });
            $input = $request->only(['password', 'email']);
            $input['grant_type'] = $social ?: 'password';
            $input['client_id'] = \Config::get("api.client_id");
            $input['client_secret'] = \Config::get("api.client_secret");
            if (is_null($input['client_secret'])) {
                throw new CustomSentryException("Client secret does not exist.");
            }
            $input['username'] = $input['email'];
            $oauth = \App::make('oauth2.authorization-server');
            $data['token'] = $oauth->issueAccessToken($input);
            $user->recordLogin();
        }
        $data['user'] = with(new UserTransformer())->transform($user);
        return $data;
    }

    public function activate($code, $orgId)
    {
        $user = $this->provider->findUserByActivationCode($code);
        $user->attemptActivation($code);
        $organizationID = $orgId ?? $user->organization->id ?? null;
        set_temporary_tenant($organizationID);
        \Event::fire('user.activated', [$user, $organizationID]);

        return $user;
    }

    function resendActivation()
    {
        $user = \API::user();
        with(new SendActivationEmail())->handle($user);
    }

    function resendActivationAsGuest($id)
    {
        $user = \Sentry::findUserById($id);
        with(new SendActivationEmail())->handle($user);
    }

    /**
     * Generete random password
     * @param Integer $length length of password
     * @param Boolean $chk_special allow special character
     * @param Boolean $chk_upper allow upper letter
     * @param Boolean $chk_lower allow lower letter
     * @param Boolean $chk_digit allow digit 
     * @return String password 
     */
    public function generateRandomPassword($length = 8, $chk_special = true, $chk_upper = true, $chk_lower = true , $chk_digit = true) {
        $count = 1;//default number of passwords
        
        $chk_safe       = true;
        $chk_require    = true;

        $str_upper   = '';
        $str_lower   = '';
        $str_digit   = '';
        $str_special = '';

        // characters that will be used
        if($chk_upper===true) {
        	$str_upper="ABCDEFGHJKLMNPQRSTUVWXYZ";
        	if($chk_safe!==true) $str_upper.="IO";
        }

        if($chk_lower===true) {
        	$str_lower.="abcdefghjkmnpqrstuvwxyz";
        	if($chk_safe!==true) $str_lower.="iol";
        }

        if($chk_digit===true) {
        	$str_digit.="23456789";
        	if($chk_safe!==true) $str_digit.="01";
        }

        if($chk_special===true) {
        	$str_special.="?=.*[#?\.!@$%^&*-]";
        }


        $use = $str_upper.$str_lower.$str_digit;
        $use_length = strlen($use)-1;

        // number of passwords to create
        for($x=0; $x<$count; ++$x) {
            $password='';

            // if requiring one of each type, preload the password with those. is randomized later with str_shuffle()
            if($chk_require===true) {
                if($chk_upper===true) $password .= $str_upper[rand(0,(strlen($str_upper)-1))];
                if($chk_lower===true) $password .= $str_lower[rand(0,(strlen($str_lower)-1))];
                if($chk_digit===true) $password .= $str_digit[rand(0,(strlen($str_digit)-1))];
                // We're requiring this, but only one of them so not included in the $use string
                if($chk_special===true) $password .= $str_special[rand(0,(strlen($str_special)-1))];
            }

            // length to make password (minus the characters already forced by require)
            for($i=strlen($password); $i<$length; ++$i) {
                $password .= $use[rand(0,$use_length)];
            }

            // shuffle the char order then print (with any special chars html safe)
            return str_shuffle($password);
        }
    }
}
