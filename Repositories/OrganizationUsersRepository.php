<?php namespace API\V1\Repositories\Eloquent;

use API\V1\Models\Organization;
use API\V1\Models\User;
use API\V1\Transformers\UserTransformer;
use API\App\Exceptions\ResourceException;

class OrganizationUsersRepository extends BaseRepository
{
    
    function __construct(User $model, UserTransformer $transformer)
    {
        $this->request = \App::make('request');
        parent::__construct($model, $transformer);
    }

    function query()
    {
        if (is_null($this->query)) {
            $this->query = Organization::where('id', \Session::get('tenant_id'))->first()->users();
        }
        return $this->query;
    }

    public function join()
    {
        $userRoleID = \API::user()->role->id;

        $orgSlug = \Input::get('company_slug');
        $joiningOrg = Organization::where('slug', $orgSlug)->firstOrFail();
        set_tenant_hard($joiningOrg->id);

        if ($joiningOrg->users->find(\API::user()->id) && is_null($joiningOrg->users->find(\API::user()->id)->pivot->approved_at))
        {
            throw new ResourceException('You request to join this organization is under review');
        }
        else if ($joiningOrg->users->contains(\API::user()->id))
        {
            throw new ResourceException('User is already a member of this organization');
        }

        $joiningOrg->users()->attach(\API::user()->id);

        $joiningOrg->users()->find(\API::user()->id)->roles()->attach($userRoleID);

        $joiningOrgAdmins = [];
        foreach ($joiningOrg->admins as $admin)
        {
            $joiningOrgAdmins[] = $admin;
        }

        return $joiningOrgAdmins;
    }
} 