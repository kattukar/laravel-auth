<?php

namespace Tallyfy\API\Providers;

use Laravel\Passport\Bridge\Scope;
use Tallyfy\API\App\Auth\OAuthProviderInterface;
use Laravel\Passport\Client as ClientModel;
use Tallyfy\API\V1\Models\User;
use DateTime;
use GuzzleHttp\Psr7\Response;
use Illuminate\Events\Dispatcher;
use Laravel\Passport\Bridge\AccessToken;
use Laravel\Passport\Bridge\AccessTokenRepository;
use Laravel\Passport\Bridge\Client;
use Laravel\Passport\Passport;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResponseTypes\BearerTokenResponse;

class OAuthProvider
{

    public function getBearerTokenByClientCredentials($client_id, $client_secret)
    {
        $user = User::where(['client_id' => $client_id, 'client_secret' => $client_secret])->firstOrFail();

        return $this->getBearerTokenByUser($user);
    }

    public function getBearerTokenByUser(User $user, $clientId = null)
    {
        if (is_null($clientId)) {
            $clientId = ClientModel::where('name', 'Tallyfy Client Credentials')->first()->id;
        }

        $passportToken = $this->createPassportTokenByUser($user, $clientId);
        $bearerToken = $this->sendBearerTokenResponse($passportToken['access_token']);

        return json_decode($bearerToken->getBody()->__toString(), true);
    }

    protected function createPassportTokenByUser(User $user, $clientId)
    {
        $scopes = [];
        foreach (['organization', 'account', 'template', 'process'] as $scope) {
            $scopes[] = (new Scope($scope));
        }
        $accessToken = new AccessToken($user->id, $scopes);
        $accessToken->setIdentifier($this->generateUniqueIdentifier());
        $accessToken->setClient(new Client($clientId, null, null));
        $accessToken->setExpiryDateTime((new DateTime())->add(Passport::tokensExpireIn()));

        $accessTokenRepository = new AccessTokenRepository(new TokenRepository(), new Dispatcher());
        $accessTokenRepository->persistNewAccessToken($accessToken);

        return [
            'access_token' => $accessToken,
        ];
    }

    protected function sendBearerTokenResponse($accessToken)
    {
        $response = new BearerTokenResponse();
        $response->setAccessToken($accessToken);

        $privateKey = new CryptKey('file://'.Passport::keyPath('oauth-private.key'), null, false);

        $response->setPrivateKey($privateKey);
        $response->setEncryptionKey(app('encrypter')->getKey());

        return $response->generateHttpResponse(new Response);
    }

    protected function generateUniqueIdentifier($length = 40)
    {
        try {
            return bin2hex(random_bytes($length));
            // @codeCoverageIgnoreStart
        } catch (\TypeError $e) {
            throw OAuthServerException::serverError('An unexpected error has occurred');
        } catch (\Error $e) {
            throw OAuthServerException::serverError('An unexpected error has occurred');
        } catch (\Exception $e) {
            // If you get this message, the CSPRNG failed hard.
            throw OAuthServerException::serverError('Could not generate a random string');
        }
    }
}