<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(function ($router) {
            $router->forAccessTokens();
            $router->forTransientTokens();
            $router->forAuthorization();
        });
        Passport::tokensCan([
            'organization' => 'Grants access to manage organization, users, tags and subscription',
            'account' => 'Grants access to manage account details, account tasks and process views',
            'template' => 'Grants access to manage templates and steps',
            'process' => 'Grants access to manage process, tasks and one-off tasks',
        ]);
    }
}
