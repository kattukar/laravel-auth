<?php namespace API\Providers;

use Cartalyst\Sentry\Sentry;

class AuthProvider
{
    protected $provider;

    function __construct(Sentry $provider)
    {
        $this->provider = $provider;
    }

    public function findUserByLogin($email)
    {
        return $this->provider->findUserByLogin($email);
    }

    public function findUserByCredentials($email, $password = '')
    {
        if ($password)
        {
            $credentials = ['email' => $email, 'password' => $password];

            return $this->provider->findUserByCredentials($credentials);
        }

        return $this->findUserByLogin($email);
    }

    public function register(array $credentials, $activate = false)
    {
        return $this->provider->register($credentials, true);
    }
}