<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\BaseControllerNew;
use API\V1\Repositories\Eloquent\UsersRepository;
use API\V1\Transformers\SupportUserTransformer;
use Input;

class UsersController extends BaseControllerNew
{
    public function __construct(SupportUserTransformer $transformer, UsersRepository $repository)
    {
        $this->setTenant = false;
        $this->transformer = $transformer;
        $this->repository =  $repository;
        parent::__construct();
    }

    public function index()
    {
        $limit = Input::get('per_page', 10);
        $sort = Input::get('sort', $this->defaultSortBy);
        $query = Input::get('q');

        $collection = $this->repository
            ->search($query)
            ->sortBy($sort)
            ->filter()
            ->getPaginated($limit);

        return $this->respondWithPaginator($collection);
    }

    public function show($id)
    {
        $item = $this->repository->findByIDOrUsername($id);
        return $this->respondWithItem($item);
    }
}