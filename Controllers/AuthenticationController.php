<?php
namespace App\Http\Controllers;

use Illuminate\Validation\ValidationException;
use Input;
use Session;
use Sentry;
use API\Providers\OAuthProvider;
use API\V1\Models\User;

class AuthenticationController extends BaseController
{
    public function __construct (OAuthProvider $oAuthProvider)
    {
        $this->oAuthProvider = $oAuthProvider;
    }

    public function recoverPassword()
    {
        $email = Input::get('email');
        try {
            $user = User::where('email', $email)->firstOrFail();
            if($user->isActivated() && !$user->isDisabledFromAllOrgs()) {
                $code = $user->getResetPasswordCode();
                // Remove session.
                Session::forget('tenant_id');
                mail_queue('emails.accounts.auth.recover_password', 'Password reset', ['user' => short_user($user), 'code' => $code], $user, null, $forceSend = true);
            }
        }
        catch (\Exception $e) {
            sendErrorLogs($e);
        }
        return ['result' => 'If you provided a valid email address, the password reset link has been sent'];
    }

    public function getToken()
    {
        return $this->oAuthProvider->getBearerTokenByUser(\API\V1\Models\User::findOrFail(request('user_id')));
    }

    public function resetPassword()
    {
        $code = request('code');

        if($code=='' or is_null($code))
            return \App::abort(422, 'Missing Reset Code');
        try {
            $user = Sentry::findUserByResetPasswordCode($code);
        }
        catch (\Exception $e) {
            sendErrorLogs($e);

            return ['error' => true ,'message'=>'Reset code invalid', 'code'=>'RESET_PASSWORD_CODE_INVALID'];
        }

        $password_policy = \Config::get('password_policies.default');

        $rules = [
            'password' => 'required|confirmed' . \Config::get('password_policies.options.' . $password_policy . '.regex'),
        ];

        $messages = [
            'password.regex' => \Config::get('password_policies.options.' . $password_policy . '.message'),
        ];

        $password = request('password');
        $validator = \Validator::make(request()->all(), $rules, $messages);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $user->attemptResetPassword($code,$password);
        \Event::fire("user.unsuspended", $user);

        mail_queue('emails.accounts.auth.reset_confirmation', 'Your password has been reset', ['user' => short_user($user)], $user, null, $forceSend = true);

        return ['error' => false, 'message' => 'Password reset successful', 'code' => 'PASSWORD_RESET_SUCCESS'];
    }

    public function resetFromAccount()
    {
        $user = Sentry::findUserByCredentials(['email' => \Request::input('email')]);

        mail_queue('emails.accounts.auth.reset_confirmation', 'Your password has been reset', ['user' => short_user($user)], $user, null, $forceSend = true);

        return ['error' => false, 'message'=>'Password reset successful', 'code' => 'PASSWORD_RESET_SUCCESS'];
    }

    public function checkResetCode($code='') {

        try {
            $user = Sentry::findUserByResetPasswordCode($code);
        }
        catch (\Exception $e) {
           return response()->json(['error'=>true, 'message' => 'reset code expired'])->setStatusCode(400);
        }

        return ['success' => true];
    }
}
