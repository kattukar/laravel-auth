<?php
namespace App\Http\Controllers\Support;

use App\Http\Controllers\BaseControllerNew;
use API\V1\Repositories\Eloquent\RolesRepository;
use API\V1\Transformers\RoleTransformer;

class RolesController extends BaseControllerNew
{
    public function __construct(RoleTransformer $transformer, RolesRepository $repo)
    {
        $this->repository = $repo;
        $this->transformer = $transformer;
        $this->filterDisabled = true;
        $this->setTenant = false;

        parent::__construct();
    }

    public function index($orgID)
    {
        set_temporary_tenant($orgID);

        $roles = $this->repository->getAllRoles($this->getQueryBuilderParams());

        return $this->respondWithPaginator($roles);
    }

}