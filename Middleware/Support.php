<?php

namespace App\Http\Middleware;

use Closure;

class Support
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! support_user()) {
            return abort(401, 'Unauthorized');
        }
        else {
            set_db_user(auth_user()->id);
        }

        return $next($request);
    }
}
