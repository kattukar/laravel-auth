<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;

class CheckForMaintenanceMode extends Middleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle($request, Closure $next)
    {
        try {
            $response = parent::handle($request, $next);
            return $response;
        }
        catch (MaintenanceModeException $exception) {
            return \Response::make(
                [
                    "message" => "Down for maintenance right now - we'll be back soon!",
                    "code" => "MAINTENANCE"
                ],
                503,
                [
                    'Access-Control-Allow-Origin' => '*',
                    'Access-Control-Allow-Headers' => 'X-Requested-With, Content-type, Authorization, Accept, If-Match, If-None-Match, Origin',
                    'Access-Control-Allow-Methods' => 'GET,HEAD,POST,PUT,PATCH,DELETE,OPTIONS'
                ]
            );
        }
    }
}
