<?php

namespace App\Http\Middleware;

use Closure;

class AfterMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if( env('DEBUG_SQL') ) {
            foreach (\DB::getQueryLog() as $log) {
                $bindings = [];
                foreach($log['bindings'] as $binding) {
                    $bindings[] = ($binding instanceof \DateTime) ? $binding->format('Y-m-d H:i:s') : $binding;
                }
                \Log::debug(vsprintf(str_replace('?', '%s', $log['query']), $bindings));
            }
            \Log::debug( 'Total count: ' . count(\DB::getQueryLog() ) );
        }

        return $response;
    }
}